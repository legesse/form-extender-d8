<?php
namespace Drupal\form_extender\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 *
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.site_information_settings'))
      $route->setDefault('_form', '\Drupal\form_extender\Form\ExtendedSettingsForm');
    }



}
