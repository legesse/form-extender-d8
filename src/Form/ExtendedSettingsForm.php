<?php

namespace Drupal\form_extender\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteInformationForm;


class ExtendedSettingsForm extends SiteInformationForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_config = $this->config('system.site');
    $form =  parent::buildForm($form, $form_state);
    $form['site_information']['siteapikey'] = [
      '#type' => 'textfield',
      '#title' => t('Site API Key'),
      '#default_value' => $site_config->get('siteapikey') ?: 'No API Key yet',
      '#description' => t("Custom field to set the API Key"),
    ];
    //Change the 'Save Configuration' label of the submit button to 'Updated Configuration'
    $form['actions']['submit']['#value'] = t('Update Configuration');
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('system.site')
      ->set('siteapikey', $form_state->getValue('siteapikey'))
      ->save();
    \Drupal::messenger()->addMessage($this->t('You have successfully saved %siteApiKey as your API key.', [
      '%siteApiKey' => $form_state->getValue('siteapikey'),
    ]));
  }
}
