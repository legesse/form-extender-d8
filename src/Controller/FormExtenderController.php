<?php
/**
 * @file
 * Contains \Drupal\site_api_key\Controller\SiteApiKeyController.
 */
namespace Drupal\form_extender\Controller;

use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


class FormExtenderController{
  /**
   *
   * @param $api_key - the API key parameter
   * @param NodeInterface $node - the node built from the node id parameter
   * @return JsonResponse
   */
  public function content($api_key, NodeInterface $nid){
    // Get API key from previous configuration data
    $api_key_db = \Drupal::config('system.site')->get('siteapikey');

    //Evaluate the node type and validity of the site API key
    if($nid->getType() == 'page' && $api_key_db != 'No API Key yet' && $api_key == $api_key_db ){
      return new JsonResponse($nid->toArray(), 200, ['Content-Type'=> 'application/json']);
    }
    //Return access denied messaged if the validation fails
    return new JsonResponse(array("error" => "Access Denied"), 401, ['Content-Type'=> 'application/json']);
  }
}
